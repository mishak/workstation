include_recipe "#{cookbook_name}::vagrant"

include_recipe "#{cookbook_name}::dev_ruby"

if node['current_user'] == 'root'
  version = node[cookbook_name]['chefdk']['version']
  remote_package 'chefdk' do
    source "https://packages.chef.io/stable/ubuntu/12.04/chefdk_#{version}_amd64.deb"
  end

  return
end

%w(
  linter-foodcritic
  linter-rubocop
  language-chef
).each do |package|
  atom_apm package
end

# patch vagrant for kitchen
home = node['etc']['passwd'][node['current_user']]['dir']

directory "#{home}/.vagrant.d"

file "#{home}/.vagrant.d/Vagrantfile" do
  content <<-EOF
  Vagrant.configure("2") do |config|
    config.ssh.insert_key = false
  end
  EOF
end
