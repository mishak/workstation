include_recipe "#{cookbook_name}::atom"
include_recipe "#{cookbook_name}::docker"

return if node['current_user'] == 'root'
# git config --global user.name "John Doe"
# git config --global user.email johndoe@example.com
# git config --global color.status auto
# git config --global color.branch auto
# git config --global color.interactive auto
# git config --global color.diff auto

%w(
  atom-terminus
  highlight-selected
  build
  build-make
  editorconfig
  git-plus
  language-env
  language-ini
  language-log
  linter-jsonlint
  show-repository-web
).each do |package|
  atom_apm package
end
