include_recipe "#{cookbook_name}::ruby"

if node['current_user'] == 'root'
  package %w(
    ruby-dev
    zlib1g-dev
  )

  gem_package 'bundler'

  return
end
