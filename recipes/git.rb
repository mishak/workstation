if node['current_user'] == 'root'
  git_client 'default'

  return
end

git_config 'user.name' do
  value node[cookbook_name]['git']['user_name']
end

git_config 'user.email' do
  value node[cookbook_name]['git']['user_email']
end

%w(status branch interactive diff).each do |field|
  git_config "color.#{field}" do
    value 'auto'
  end
end

git_config 'push.default' do
  value 'simple'
end
