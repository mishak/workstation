return unless node['current_user'] == 'root'

apt_repository 'docker' do
  uri 'https://apt.dockerproject.org/repo'
  distribution "ubuntu-#{node['lsb']['codename']}"
  components %w(main)
  key '58118E89F3A912897C070ADBF76221572C52609D'
  keyserver 'hkp://p80.pool.sks-keyservers.net:80'
end

package 'docker-engine'

service 'docker' do
  action [:enable, :start]
end

remote_file '/usr/local/bin/docker-compose' do
  version = node[cookbook_name]['docker-compose']['version']
  source "https://github.com/docker/compose/releases/download/#{version}/docker-compose-Linux-x86_64"
  checksum node[cookbook_name]['docker-compose']['checksum']
  mode 0755
end

remote_file '/usr/local/bin/docker-machine' do
  version = node[cookbook_name]['docker-machine']['version']
  source "https://github.com/docker/machine/releases/download/v#{version}/docker-machine-Linux-x86_64"
  checksum node[cookbook_name]['docker-machine']['checksum']
  mode 0755
end

package 'unzip'

version = node[cookbook_name]['docker-ls']['version']
temp_file = "#{Chef::Config['file_cache_path']}/docker-ls-linux-amd64-v#{version}.zip"

remote_file temp_file do
  action :create_if_missing
  source "https://github.com/mayflower/docker-ls/releases/download/v#{version}/docker-ls-linux-amd64.zip"
  checksum node[cookbook_name]['docker-ls']['checksum']
  mode 0644
end

execute "unpack docker-ls" do
  command "unzip -o #{temp_file} -d /usr/local/bin"
  not_if { ::File.zero?(temp_file) }
end

file temp_file do
  content ''
  not_if { ::File.zero?(temp_file) }
end

file '/usr/local/bin/docker-ls' do
  mode 0755
end

file '/usr/local/bin/docker-rm' do
  mode 0755
end
