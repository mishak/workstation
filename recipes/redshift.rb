if node['current_user'] == 'root'
  package 'redshift-gtk'

  return
end

home = node['etc']['passwd'][node['current_user']]['dir']

file "#{home}/.config/redshift.conf" do
  content <<-EOF
  [redshift]
  location-provider=manual

  [manual]
  lat=#{node[cookbook_name]['redshift']['lat']}
  lon=#{node[cookbook_name]['redshift']['lon']}
  EOF
end

gnome_autostart 'redshift-gtk' if node[cookbook_name]['desktop'] == 'gnome-shell'
