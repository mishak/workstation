# Manage go versions using Go Version Manager
# https://github.com/moovweb/gvm

if node['current_user'] == 'root'
  package 'go' do
    action :remove
  end

  # dependencies
  package 'bison'

  return
end

remote_file "#{Chef::Config['file_cache_path']}/gvm-installer" do
  source "https://raw.githubusercontent.com/moovweb/gvm/#{node[cookbook_name]['go']['gvm_commit']}/binscripts/gvm-installer"
  checksum node[cookbook_name]['go']['gvm_checksum']
  mode 0755
end

home = node['etc']['passwd'][node['current_user']]['dir']
shell = node['etc']['passwd'][node['current_user']]['shell']
install_dir = "#{home}/.gvm"
gvm = "#{install_dir}/bin/gvm"
environment = {
  'GVM_ROOT' => install_dir
}

execute 'install' do
  command "'#{shell}' #{Chef::Config['file_cache_path']}/gvm-installer"
  creates install_dir
end

method = node[cookbook_name]['go']['method']
gvm_options = ''
gvm_options = ' -B' if method == 'binary'
default = node[cookbook_name]['go']['version']
[node[cookbook_name]['go']['alternatives'] + [default]].flatten.uniq
                                                       .each do |version|
  Chef::Log.warn("Go version: #{version}")
  execute "install #{version}" do
    command "'#{gvm}' install 'go#{version}'#{gvm_options}"
    environment environment
    not_if "'#{gvm}' list \
| grep '^\\(=>\\|  \\) go#{version.gsub('.', '\\.')}$'",
           environment: environment
  end
end

%w(
  go-plus
  gometalinter-linter
).each do |package|
  atom_apm package
end
