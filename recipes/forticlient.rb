return unless node['current_user'] == 'root'

remote_package 'forticlient-sslvpn' do
  version = node[cookbook_name]['forticlient-sslvpn']['version']
  source "https://hadler.me/files/forticlient-sslvpn_#{version}_amd64.deb"
  checksum node[cookbook_name]['forticlient-sslvpn']['checksum']
  filename "forticlient-sslvpn_#{version}_amd64.deb"
end
