# core tools
package %w(
  curl
  jq
)

# CLI tools
package %w(
  htop
  mc
  mitmproxy
  tig
  tmux

  tree
)

package %w(
  graphviz
  meld
)
