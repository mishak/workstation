return if node['current_user'] == 'root'

package 'openssh-client'

execute 'generate private key' do
  home = node['etc']['passwd'][node['current_user']]['dir']
  pk = "#{home}/.ssh/id_rsa"
  command "ssh-keygen -t rsa -b 4096 -N '' -f '#{pk}'"
  creates pk
end
