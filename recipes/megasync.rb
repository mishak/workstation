return unless node['current_user'] == 'root'

apt_repository 'megasync' do
  uri 'http://mega.nz/linux/MEGAsync/xUbuntu_16.04/'
  distribution './'
  components []
end

package %w(
  megasync
  nautilus-megasync
)
