return unless node['current_user'] == 'root'

version = node[cookbook_name]['virtualbox']['version']
min_maj_patch = version.split('-')[0]
min_maj = min_maj_patch.split('.')[0..1].join('.')
remote_package "virtualbox-#{min_maj}" do
  source "http://download.virtualbox.org/virtualbox/#{min_maj_patch}/virtualbox-#{min_maj}_#{version}~Ubuntu~xenial_amd64.deb"
  fix_broken true
end
