return if node['current_user'] == 'root'

home = node['etc']['passwd'][node['current_user']]['dir']

gnome_setting 'org.gnome.desktop.wm.preferences button-layout' do
  value 'appmenu:minimize,close'
end

gnome_setting 'org.gnome.desktop.wm.preferences action-middle-click-titlebar' do
  value 'lower'
end

gnome_setting 'org.gnome.desktop.peripherals.touchpad tap-to-click' do
  value true
end

file "#{home}/.config/gtk-3.0/settings.ini" do
  content <<EOL
[Settings]
gtk-application-prefer-dark-theme=1
EOL
  mode 0644
end

gnome_setting 'org.gnome.Terminal.Legacy.Settings default-show-menubar' do
  value false
end

gnome_extension 'alternate-tab@gnome-shell-extensions.gcampax.github.com'
gnome_extension 'drive-menu@gnome-shell-extensions.gcampax.github.com'
gnome_extension 'windowsNavigator@gnome-shell-extensions.gcampax.github.com'

atom_apm 'gtk-dark-theme'

gnome_setting 'org.gnome.shell favorite-apps' do
  value %w(
    google-chrome
    atom
    org.gnome.Nautilus
    gnome-terminal
  ).map { |x| x + '.desktop' }
end
