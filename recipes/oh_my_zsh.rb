include_recipe "#{cookbook_name}::zsh"

return if node['current_user'] == 'root'

home = node['etc']['passwd'][node['current_user']]['dir']

git "#{home}/.oh-my-zsh" do
  action :checkout
  repository 'git://github.com/robbyrussell/oh-my-zsh.git'
end

remote_file "#{home}/.zshrc" do
  action :create_if_missing
  source "file:///home/#{node['current_user']}/.oh-my-zsh/templates/zshrc.zsh-template"
end

replace "#{home}/.zshrc" do
  replace(/^# DISABLE_AUTO_UPDATE=.*$/)
  with 'DISABLE_AUTO_UPDATE="true"'
end
