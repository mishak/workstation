include_recipe "#{cookbook_name}::git"

if node['current_user'] == 'root'
  remote_package 'atom' do
    version = node[cookbook_name]['atom']['version']
    source "https://atom-installer.github.com/v#{version}/atom-amd64.deb"
    filename "atom-#{version}-amd64.deb"
  end

  return
end

%w(
  editorconfig
  file-icons
  linter-alex
  linter-write-good
  sort-lines
  split-diff
  highlight-selected
).each do |package|
  atom_apm package
end
