return unless node['current_user'] == 'root'

# disable repository management by google-chrome-* package
file '/etc/default/google-chrome'

apt_repository 'google-chrome' do
  uri 'http://dl.google.com/linux/chrome/deb/'
  arch 'amd64'
  distribution 'stable'
  components %w(main)
  key 'https://dl.google.com/linux/linux_signing_key.pub'
end

package 'google-chrome-stable'
