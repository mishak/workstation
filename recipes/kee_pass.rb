return unless node['current_user'] == 'root'

package [
  node[cookbook_name]['kee_pass']['package'],
  node[cookbook_name]['kee_pass']['dependencies']
].flatten

directory node[cookbook_name]['kee_pass']['plugins_dir']

node[cookbook_name]['kee_pass']['plugins'].each do |name, info|
  remote_file "#{node[cookbook_name]['kee_pass']['plugins_dir']}/#{name}.plgx" do
    source info['source'].gsub('${version}', info['version'])
    checksum info['checksum'] if info.attribute? 'checksum'
    mode 0644
  end
end
