include_recipe "#{cookbook_name}::essentials"

include_recipe "#{cookbook_name}::identity"

include_recipe "#{cookbook_name}::git"
include_recipe "#{cookbook_name}::vim"
include_recipe "#{cookbook_name}::zsh"

include_recipe "#{cookbook_name}::oh_my_zsh"

include_recipe "#{cookbook_name}::redshift"

include_recipe "#{cookbook_name}::hack_font"

include_recipe "#{cookbook_name}::atom"
include_recipe "#{cookbook_name}::docker"
include_recipe "#{cookbook_name}::forticlient"
include_recipe "#{cookbook_name}::google_chrome"
include_recipe "#{cookbook_name}::kee_pass"
include_recipe "#{cookbook_name}::megasync"

include_recipe "#{cookbook_name}::dev"
include_recipe "#{cookbook_name}::dev_chef"
include_recipe "#{cookbook_name}::dev_docker"

include_recipe "#{cookbook_name}::gnome_shell" if node[cookbook_name]['desktop'] == 'gnome-shell'
