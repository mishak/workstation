return if node['current_user'] == 'root'

%w(
  build-dockerfile
  linter-docker
  language-docker
).each do |package|
  atom_apm package
end
