return unless node['current_user'] == 'root'

package 'zsh'

node[cookbook_name]['zsh']['users'].each do |name|
  user name do
    shell '/bin/zsh'
  end
end
