return unless node['current_user'] == 'root'

package 'vim'

execute 'update-alternatives' do
  path = '/usr/bin/vim.basic'
  command "update-alternatives --set editor '#{path}'"
  not_if "update-alternatives --query editor | grep -F 'Value: #{path}'"
end
