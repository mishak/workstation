include_recipe "#{cookbook_name}::virtualbox"

return unless node['current_user'] == 'root'

remote_package 'vagrant' do
  version = node[cookbook_name]['vagrant']['version']
  source "https://releases.hashicorp.com/vagrant/#{version}/vagrant_#{version}_x86_64.deb"
end
