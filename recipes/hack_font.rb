if node['current_user'] == 'root'
  package 'fonts-hack-ttf'

  return
end

gnome_setting 'org.gnome.desktop.interface monospace-font-name' do
  value 'Hack 12'
  only_if { node[cookbook_name]['desktop'] == 'gnome-shell' }
end
