#!/bin/bash
set -euo pipefail

main() {
	local recipe=""
	local cookbook
	cookbook="$(basename "$(pwd)")"
	if [ $# -eq 1 ]; then
		recipe="::$1"
	fi

	# install dependecies
	[ -f ./Berksfile.lock ] || berks install
	# copy dependencies to ./cookbooks directory
	rm -rf cookbooks
	berks vendor cookbooks

	# first pass as super user
	sudo chef-client \
		--local-mode \
		--disable-config \
		--runlist "${cookbook}${recipe}"

	# fix rights
	sudo chmod -R a+rwX "$HOME/.chef/local-mode-cache" ./nodes

	# second pass as user
	chef-client \
		--local-mode \
		--disable-config \
		--runlist "${cookbook}${recipe}"
}

main "$@"
