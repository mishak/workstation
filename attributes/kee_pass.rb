default['workstation']['kee_pass']['package'] = 'keepass2'
default['workstation']['kee_pass']['dependencies'] = %w(
  libmono-system-xml-linq4.0-cil
  libmono-system-data-datasetextensions4.0-cil
  libmono-system-runtime-serialization4.0-cil
  mono-mcs
)

default['workstation']['kee_pass']['plugins_dir'] = '/usr/lib/keepass2/Plugins'

default['workstation']['kee_pass']['plugins']['TrayTotp']['source'] = 'https://sourceforge.net/projects/traytotp-kp2/files/Tray%20TOTP%20v.%20${version}/TrayTotp.plgx'
default['workstation']['kee_pass']['plugins']['TrayTotp']['version'] = '2.0.0.5'
default['workstation']['kee_pass']['plugins']['TrayTotp']['checksum'] = '6373bb8f91375022c65169c272eb1b262001f0b19e04c459386c65e4ad6263bf'

default['workstation']['kee_pass']['plugins']['KeePassHttp']['source'] = 'https://github.com/pfn/keepasshttp/raw/${version}/KeePassHttp.plgx'
default['workstation']['kee_pass']['plugins']['KeePassHttp']['version'] = '1.8.4.1'
default['workstation']['kee_pass']['plugins']['KeePassHttp']['checksum'] = '430948219b0bc282fbf0760da7b66f72b13eee4db57de457e563342d253bd8ae'
