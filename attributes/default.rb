default['workstation']['desktop'] = 'gnome-shell'

default['workstation']['git']['user_name'] = 'Michal Gebauer'
default['workstation']['git']['user_email'] = 'mishak@mishak.net'

default['workstation']['atom']['version'] = '1.12.7'
default['workstation']['chefdk']['version'] = '0.18.30-1'
default['workstation']['forticlient-sslvpn']['version'] = '4.4.2330-1'
default['workstation']['forticlient-sslvpn']['checksum'] = \
  'ebeec66baf5d15c40eaf1b9f4c721b29252d6314a354e5798ca6a036279e8640'
default['workstation']['vagrant']['version'] = '1.8.6'
default['workstation']['virtualbox']['version'] = '5.1.8-111374'

default['workstation']['zsh']['users'] = ['mishak']
