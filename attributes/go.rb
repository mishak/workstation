default['workstation']['go']['method'] = 'binary' # or 'source'
default['workstation']['go']['version'] = '1.8'
default['workstation']['go']['alternatives'] = %w(
  1.7.3
  1.6.3
  1.5.4
  1.4.3
)

default['workstation']['go']['gvm_commit'] = \
  'f38923cc7b3108747b67ff8d0d633569b36cf99b'
default['workstation']['go']['gvm_checksum'] = \
  'da9140a8f91309655a6018517244478c4efa748f91bdcee5d8a5d4457a8b172b'
