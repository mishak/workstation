resource_name :gnome_extension
default_action :enable
property :key, kind_of: String, name_property: true
property :value, kind_of: [TrueClass, FalseClass, String], required: true

use_inline_resources if defined?(use_inline_resources)

include Workstation::Helpers

EXTENSIONS_KEY = 'org.gnome.shell enabled-extensions'.freeze

def list_extensions
  gsettings_get(EXTENSIONS_KEY)
end

action :enable do
  extensions = list_extensions
  if extensions.index(name).nil?
    extensions << name
    gsettings_set(EXTENSIONS_KEY, extensions)
  end
end

action :disable do
  extensions = list_extensions
  unless extensions.index(name).nil?
    extensions.select! { |x| x != name }
    gsettings_set(EXTENSIONS_KEY, extensions)
  end
end
