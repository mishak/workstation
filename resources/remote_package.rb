resource_name :remote_package

property :name, kind_of: String, name_property: true
property :source, kind_of: String
property :fix_broken, kind_of: [TrueClass, FalseClass], default: false
property :checksum, kind_of: String, required: false
property :filename, kind_of: String, required: false
property :empty_on_success, kind_of: [TrueClass, FalseClass], default: true

default_action :install

use_inline_resources if defined?(use_inline_resources)

action :install do
  basename = if new_resource.filename.nil? || new_resource.filename.empty?
               ::File.basename(source)
             else
               new_resource.filename
             end
  temp_file = "#{Chef::Config['file_cache_path']}/#{basename}"

  remote_file temp_file do
    action :create_if_missing
    source new_resource.source
    checksum new_resource.checksum
    mode 0644
  end

  package name do
    action :upgrade
    source temp_file
    provider Chef::Provider::Package::Dpkg if node['platform_family'] == 'debian'
    if fix_broken
      ignore_failure true
      notifies :run, "execute[install dependencies #{name}]", :immediately
    end
    not_if { ::File.zero?(temp_file) }
  end

  execute "install dependencies #{name}" do
    command 'apt-get install --fix-broken --yes'
    not_if { ::File.zero?(temp_file) }
  end if fix_broken

  file temp_file do
    content ''
    not_if { ::File.zero?(temp_file) }
  end if empty_on_success
end
