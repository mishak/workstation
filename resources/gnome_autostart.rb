resource_name :gnome_autostart

property :application, kind_of: String, name_property: true

default_action :enable

use_inline_resources if defined?(use_inline_resources)

include Workstation::Helpers

def home
  node['etc']['passwd'][node['current_user']]['dir']
end

def autostart_directory()
  "#{home}/.config/autostart"
end

def autostart_filename(name)
  "#{autostart_directory}/#{name}.desktop"
end

def desktop_filename(name)
  "/usr/share/applications/#{name}.desktop"
end

action :enable do
  directory autostart_directory

  remote_file autostart_filename(application) do
    source "file://#{desktop_filename(application)}"
  end
end

action :disable do
  file autostart_filename(application) do
    action :delete
  end
end
