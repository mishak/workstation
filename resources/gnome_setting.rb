resource_name :gnome_setting
default_action :update
property :key, kind_of: String, name_property: true
property :value, kind_of: [TrueClass, FalseClass, Array, String], required: true

use_inline_resources if defined?(use_inline_resources)

include Workstation::Helpers

load_current_value do
  value gsettings_get(key)
end

action :update do
  require 'json'
  converge_if_changed do
    execute gsettings_set_command(key, value)
  end
end
