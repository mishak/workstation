name 'workstation'
maintainer 'Michal Gebauer'
maintainer_email 'mishak@mishak.net'
license 'apachev2'
description 'Installs/Configures workstation'
long_description 'Installs/Configures workstation'
version '0.0.8'

issues_url 'https://gitlab.com/mishak/workstation/issues' if respond_to?(:issues_url)
source_url 'https://gitlab.com/mishak/workstation/' if respond_to?(:source_url)

depends 'atom', '~> 0.2.0'
depends 'git', '~> 5.0.1'
depends 'patch', '~> 2.2.0'
depends 'zipfile', '~> 0.1.0'
depends 'hostsfile', '~> 2.4.5'
