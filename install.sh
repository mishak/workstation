#!/bin/bash
set -euo pipefail

if ! command -v git > /dev/null; then
	sudo apt-get update && sudo apt-get install -y git
fi

mkdir -p ~/workspace/mishak

cd ~/workspace/mishak
[[ -d workstation ]] || git clone https://gitlab.com/mishak/workstation

cd workstation
if ! command -v chef-client > /dev/null; then
	version=0.19.6-1
	file="/tmp/chefdk_${version}_amd64.deb"
	[[ -f "$file" ]] || wget "https://packages.chef.io/stable/ubuntu/12.04/chefdk_${version}_amd64.deb" -O "$file"
	sudo dpkg --install "$file"
fi

./run.sh default
