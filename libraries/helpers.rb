module Workstation
  module Helpers
    require 'chef/mixin/shell_out'
    require 'json'

    include Chef::Mixin::ShellOut

    def gsettings_get(key)
      output = shell_out("gsettings get #{key}").stdout
      output.sub!(/^@as /, '')
      JSON.parse(output.strip.tr('\'', '"'), quirks_mode: true)
    end

    def gsettings_set(key, value)
      shell_out(gsettings_set_command(key, value)).error!
    end

    def gsettings_set_command(key, value)
      "gsettings set #{key} \"#{value.to_json.tr('"', '\'')}\""
    end
  end
end
